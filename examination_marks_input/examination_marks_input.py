# Copyright (c) 2013, Techbeeo and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe

def execute(filters=None):
	columns, data = [], []
	columns=add_columns()
	data=reportResult(filters)
	return columns, data

def add_columns():
	return [
		"Progrm Name::120",
		"Semester Name::120",
		"Section Name::100",
		"Student ID:Link/Student Information:100",
		"Student Name::100",
		"Subject ID:Link/Subject:100",
		"Subject Name::150",
		"AT::50",
		"PR::50",
		"CT::50",
		"L::50",
		"MT::50",
		"F::50",
		"Total Marks::100",
		"Grading Point::100",
	]

def reportResult(filters):

 	

	#sqlQr =frappe.db.sql(""" SELECT `semester`,`semester_name`,`program`,`program_name`,`subject`,`subject_name`,`section`,`section_name`  
		#FROM `tabExamination Marks Input`  """)

	#return sqlQr


	condition=""
	if filters.get("semester"):
		condition += "AND semester = '{}'".format(filters['semester'])
	if filters.get("programs"):
		condition += "AND program = '{}'".format(filters['programs'])
	if filters.get("subject"):
		condition += "AND subject ='{}'".format(filters['subject'])
	if filters.get("section"):
		condition += "AND section='{}'".format(filters['section'])
	
	# Marks Input result from database child table 
	sqlQr= frappe.db.sql("""SELECT `tabExamination Marks Input`.`program_name`, `tabExamination Marks Input`.`semester_name`,`tabExamination Marks Input`.`section_name`, `tabExamination Marks Input Child`.`student_id`,`tabExamination Marks Input Child`.`student_name`,`tabExamination Marks Input`.`subject`,`tabExamination Marks Input`.`subject_name`, `tabExamination Marks Input Child`.`attendance_mark`, `tabExamination Marks Input Child`.`presentation_mark`,`tabExamination Marks Input Child`.`class_test_mark`,`tabExamination Marks Input Child`.`lab_mark`,`tabExamination Marks Input Child`.`mid_term_mark`,`tabExamination Marks Input Child`.`final_mark`,`tabExamination Marks Input Child`.`mark`,`tabExamination Marks Input Child`.`grading_point`
		FROM `tabExamination Marks Input` 
		LEFT JOIN `tabExamination Marks Input Child`
		ON `tabExamination Marks Input Child`.`parent` = `tabExamination Marks Input`.`name`
		WHERE `tabExamination Marks Input`.name !='' {fill}""".format(fill=condition))

	
	return sqlQr

