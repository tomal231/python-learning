// Copyright (c) 2013, Techbeeo and contributors
// For license information, please see license.txt

frappe.query_reports["Examination Marks Input"] = {
	"filters": [
		{
			"fieldname":"semester",
			"label": __("Semester"),
			"fieldtype": "Link",
			"options": "Semester",
		},
		{
			"fieldname":"programs",
			"label": __("Programs"),
			"fieldtype": "Link",
			"options": "Programs",
		},
		{
			"fieldname":"subject",
			"label": __("Subject"),
			"fieldtype": "Link",
			"options": "Subject",
		},
		{
			"fieldname":"section",
			"label": __("Section"),
			"fieldtype": "Link",
			"options": "Section",
		}
	]
}
